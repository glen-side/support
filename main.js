var cssroot = document.documentElement;
var rootGET = getComputedStyle(cssroot); // follow with: getPropertyValue()
var rootSET = cssroot.style; // follow with: setProperty()

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

var orz_timeOG = Date.now();
var cancTime = 16699; // estimated pageload time

document.addEventListener("DOMContentLoaded",() => {
    
    
    var bob = document.getElementsByTagName("html")[0];
    
    if(customize_page){
        bob.setAttribute("customize-page","true")
    }
    
    // which element you want to get height of
    var div = document.querySelectorAll("[top-bar]");
    
    var setTimer = setInterval(() => {
    	if(Date.now() - orz_timeOG > cancTime){
    		clearInterval(setTimer);
    	} else {
    		// .each() but in vanilla JS-
    		// for the element you targeted earlier
    		div.forEach(function(div_each){
    			if(div_each.offsetHeight > 0){
    				rootSET.setProperty("--TopBar-Height",div_each.offsetHeight + "px");
    				clearInterval(setTimer);
    			}
    		});
    	}
    },0);
});// end dom content loaded

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();
    
    $("[lower-main] h2[line-decor]").each(function(){
        $(this).addClass("flex");
        $(this).wrapInner("<span text></span>")
        $(this).prepend("<div h2-line-left></div>")
        $(this).append("<div h2-line-right></div>")
    })
    
    var navRow = "[side-nav]";
    $(navRow).each(function(){
        $(this).not(navRow + "+" + navRow).each(function(){
            $(this).nextUntil(":not(" + navRow + ")").andSelf()
            .wrapAll('<div side-nav-cont>');
        });
    })
    
    $("[side-nav]").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        
        $(this).find(".iconsax").each(function(){
            $(this).nextAll().wrapAll("<div nav-text></div>")
        })
    })
    
    // top bar height
    var tbhz;
    var headerHeight;
    
    var smcms = Date.now();
    var lqxki = 6699;
    
    var sdofc = setInterval(() => {
    	if(Date.now() - smcms > lqxki){
    		clearInterval(sdofc);		
    	} else {
    		var ltxgq = parseFloat(rootGET.getPropertyValue("--TopBar-Height"));
    		var vxaii = $("[header]").height();
    		
    		if(ltxgq > 0 && vxaii > 0){
    		    tbhz = ltxgq;
    		    headerHeight = vxaii;
    		    clearInterval(sdofc);
    		    autoHashJump();
    		}
    	}
    },0);
    
    var headerBorderSize = parseFloat(rootGET.getPropertyValue("--Header-Border-Size"));
    
    var ztickyg = parseFloat(rootGET.getPropertyValue("--Container-Padding-Y"));
    
    var pagejumpspeed = parseInt($.trim(parseFloat(rootGET.getPropertyValue("--Page-Scroll-Speed"))));
    
    /*------ SHOW SELECTED SECTION, HIDE REST ------*/
    var showDisF = $("[side-nav][anchor-id][show-this]:first").attr("anchor-id");
    $("[section][anchor-id]").each(function(){
        if($(this).attr("anchor-id") !== showDisF){
            $(this).hide();
        }
    })
    
    /*------ ANCHOR CLICK, SHOW THAT SECTION ------*/
    var secFadeSP = parseFloat(rootGET.getPropertyValue("--Section-Fade-Speed"));
    
    $("body").prepend("<div id='pseudo-body'></div>")
    
    // set 1st "article" as the "currently viewing"
    $("#pseudo-body").attr("current-section",$("[section][anchor-id]:first").attr("anchor-id"));
    
    $("[side-nav][anchor-id], a[anchor-id]").click(function(){
        var that = this;
        var aaID = $.trim($(this).attr("anchor-id"));
        
        // change browser url to match the "#" name
        var newHashURL = window.location.protocol + "//" + window.location.host + window.location.pathname + "#" + aaID;
        window.history.pushState({
            path: newHashURL
        }, "", newHashURL);
        
        // display a DIFFERENT ARTICLE
        if(aaID !== $("#pseudo-body").attr("current-section")){
            $("#pseudo-body").attr("switch-to",aaID);
            
            var distScrolled = $(document).scrollTop();
            var scrollAmt = Math.floor(headerHeight + headerBorderSize);
            
            // if page HASN'T already been scrolled,
            // SCROLL IT
            if(distScrolled !== scrollAmt){
                $("body").animate({
                    scrollTop:scrollAmt
                },pagejumpspeed);
            }
            
            // fadeOut irrelevant sections
            $("[section]:not([anchor-id='" + aaID + "'])").fadeOut(secFadeSP/2);
            
            // fade in current selection
            setTimeout(() => {
                $("[section][anchor-id='" + aaID + "']").fadeIn(secFadeSP/2)
            },secFadeSP/2)
            
            // temporarily disable scroll
            var blockScroll = mouseWheel => mouseWheel.preventDefault();
            
            window.addEventListener("wheel", blockScroll, {
        		passive: false
        	});
        	
        	// re-enable scroll
        	setTimeout(() => {
        		window.removeEventListener("wheel", blockScroll);
        	},secFadeSP)
            
            // update currentID after animation is complete
            setTimeout(() => {
                $("#pseudo-body").attr("current-section",aaID);
            },secFadeSP)
        }
        
    })//end anchor click
    
    /*---- SHOW SECTION THAT MATCHES URL'S HASH ----*/
    /*-- anchor id --*/
    function autoHashJump(){
        var c_c = window.location.hostname;
        
        if(c_c !== "glenthemes.tumblr.com"){
            $("body").remove(); // delete later
        }
        
        if(window.location.href.indexOf("#") > -1){
            var hashLess = window.location.hash.split("#")[1];
            
            if(c_c == "glenthemes.tumblr.com"){
                if(hashLess == "tutorial1"){
                    hashLess = "theme"
                } else if(hashLess == "tutorial2"){
                    hashLess = "page"
                } else {
                    hashLess = hashLess
                }
            }
            
            $("[side-nav][anchor-id='" + hashLess + "']").click()
        }
    }
    
    /*---- DISCORD WIDGET ----*/
    $("[discord-widget] [invite-msg]").nextAll().wrapAll("<div flex dc-flex space-between></div>");
    
    $("[discord-widget] [server-icon]").nextUntil("[invite-link]").wrapAll("<div usdiw>");
    
    $("[online-num] + [member-num]").each(function(){
        $(this).add($(this).prev()).wrapAll("<div flex>")
    })
    
    $("[list-row]").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3
        }).wrap("<span>");
    })
    
    $("[list-row] [label]").each(function(){
        $(this).nextAll().wrapAll("<span>");
        $(this).prevAll("span").each(function(){
            if($.trim($(this).html()) == ""){
                $(this).remove()
            }
        })
    })
    
    $("[color]").each(function(){
        var discol = $.trim($(this).attr("color"));
        if(discol !== ""){
            $(this).css("color",discol)
        }
    })
    
    $("[max-width]").each(function(){
        var maxwa = $.trim($(this).attr("max-width"));
        if(maxwa !== ""){
            $(this).css("max-width",maxwa)
        }
    })
    
    /*-------- EMOJIS --------*/
    $("img[emoji]").each(function(){
        var ejej = $.trim($(this).attr("emoji"));
        if(ejej !== ""){
            $(this).attr("src",ejej)
        }
    })
    
    /*-------- TOOLTIPS --------*/
    $("a[title]").each(function(){
        if($.trim($(this).attr("title")) !== ""){
            $(this).style_my_tooltips({
                tip_follows_cursor:true,
                tip_delay_time:0,
                tip_fade_speed:0,
                attribute:"title"
            });
        }
        
        if($(this).is("[href]")){
            if($.trim($(this).attr("href")) == ""){
                $(this).css("cursor","help")
            }
        } else {
            $(this).css("cursor","help")
        }
    })
    
});//end ready
